package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {

        int age;
        char middleName;

        // Variable Declaration vs Initialization
        int x;
        int y = 0;

        // Initialization after Declaration
        x = 1;

        // Output to the system
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data Types
        // predefined within the Java programming language which is used for single-valued within limited capabilities.

        // 1. int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // 2. long
        // L is being added at the end of the long number to be recognized
        long worldPopulation = 78628811457878L;
        System.out.println(worldPopulation);

        // 3. float - with decimals
        // add f at the end of the float to be recognized.
        float piFloat = 3.141559265359f;
        System.out.println(piFloat);

        // 4. double - floating point values
        double piDouble = 3.141559265359;
        System.out.println(piDouble);

        // 5. char - single characters
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        // 6. boolean - true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constant - naming convention use CAPITAL letter
        // Java uses the "final" keyword so the variable's value cannot be changed.
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        // PRINCIPAL = 4000; - this will result to an error

        // Non-primitive Data Types

        // 1. String
        // Stores a sequence or array of characters
        // Strings are actually object that can use methods

        String username = "JSmith";
        System.out.println(username);

        // Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);

    }
}
