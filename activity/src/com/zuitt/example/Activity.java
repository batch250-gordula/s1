package com.zuitt.example;

import java.util.Scanner;
public class Activity {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter your First Name: ");
        String firstName = myObj.nextLine();

        System.out.println("Enter your Last Name: ");
        String lastName = myObj.nextLine();

        System.out.println("Enter your First Subject Grade: ");
        double firstSubjGrade = Double.parseDouble(myObj.nextLine());

        System.out.println("Enter your Second Subject Grade: ");
        double secondSubjGrade = Double.parseDouble(myObj.nextLine());

        System.out.println("Enter your Third Subject Grade: ");
        double thirdSubjGrade = Double.parseDouble(myObj.nextLine());

        System.out.println("Good day, " + firstName + " " + lastName + ".");

        double averageGrade = (firstSubjGrade + secondSubjGrade + thirdSubjGrade) / 3;
        System.out.println("Your grade average is: " + averageGrade);

    }
}
